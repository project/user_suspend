<?php

/**
 * @file
 * Provides an interim step between account blocking and account deletion.
 */

define('USER_SUSPEND_DEFAULT_EMAIL', '!username,

Your account on !site has been suspended. You will not be able to login with your username or password until the account is reinstated.


--  !site team');
define('USER_SUSPEND_REINSTATE_EMAIL', '!username,

Your account on !site has been reinstated. You will now be able to login with your previous username or password.


--  !site team');

/**
 * Implementation of hook_help().
 */
function user_suspend_help($section) {
  switch ($section) {
    case 'admin/modules#description':
      return t('Provides an interim step between account blocking and account deletion.');

    case 'admin/help#user_suspend':
      return '<p>' . t('User Suspend provides an interim step between account blocking and account deletion. Unlike setting a user\'s status to Blocked, the user record, user roles and optionally user-created nodes and comments are transferred to separate tables where they are not accessible by the normal Drupal system. Users can be suspended via Update options on the !user_list and viewed on the !user_suspend_list.', array('!user_list' => l('Users list', 'admin/user/user'), '!user_suspend_list' => l('Suspended Users list', 'admin/user/user/suspend'))) . '</p><p>' . t('Suspended users can not login to their accounts, nor can they create new accounts with the same user ID or email address. Suspended users can be reinstated at any time, recovering all of their previous settings, roles, nodes and comments. Deleting suspended users will completely remove the user accounts and user-created nodes and comments from the system. Once deleted, a user will be able to create a new account with the same user ID and/or email address.') . "</p>\n";

    case 'admin/user/settings/user_suspend':
      return t('User Suspend provides an interim step between account blocking and account deletion. Unlike setting a user\'s status to Blocked, the user record, user roles and optionally user-created nodes and comments are transferred to separate tables where they are not accessible by the normal Drupal system. Users can be suspended via Update options on the !user_list and viewed on the !user_suspend_list.', array('!user_list' => l('Users list', 'admin/user/user'), '!user_suspend_list' => l('Suspended Users list', 'admin/user/user/suspend')));

    case 'admin/user/user/suspend':
      return t('Suspended users can not login to their accounts, nor can they create new accounts with the same user ID or email address. Suspended users can be reinstated at any time, recovering all of their previous settings, roles, nodes and comments. Deleting suspended users will completely remove the user accounts and user-created nodes and comments from the system. Once deleted, a user will be able to create a new account with the same user ID and/or email address.');
  }
}

/**
* Implementation of hook_menu().
*
*/
function user_suspend_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/user/settings/user_suspend',
      'title' => 'User suspend',
      'description' => t('Provides an interim step between account blocking and account deletion.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => 'user_suspend_admin_settings',
      'type' => MENU_NORMAL_ITEM,
	    'access' => user_access('administer site configuration'), //do we *really* need yet another permission??
    );
		$items[] = array(
      'path' => 'admin/user/user/suspend',
      'title' => t('Suspended Users'),
      'callback' => 'drupal_get_form',
      'callback arguments' => 'user_suspend_admin',
      'access' => user_access('administer users'),
      'type' => MENU_LOCAL_TASK,
      'weight' => -9
    );

  }
  return $items;
}

/**
 * Menu callback
 *
 * @return
 *   array of form content.
 */
function user_suspend_admin_settings() {
  _user_suspend_schema_diff();

  $form['user_suspend_remove_nodes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove user nodes?'),
    '#default_value' => variable_get('user_suspend_remove_nodes', 1),
    '#description' => t('If enabled, nodes created by the suspended user will be removed from the node table, but can be restored if a user is ever reinstated. If disabled, nodes created by the suspended user will remain in the nodes table, but be permanently orphaned (uid set to 0).')
  );
  $form['user_suspend_remove_comments'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove user comments?'),
    '#default_value' => variable_get('user_suspend_remove_comments', 1),
    '#description' => t('If enabled, comments created by the suspended user will be removed from the comments table, but can be restored if a user is ever reinstated. If disabled, comments created by the suspended user will remain in the comments table, but be permanently orphaned (uid set to 0).')
  );

  $form['suspend'] = array('#type' => 'fieldset', '#title' => t('User suspend e-mail settings'));
  $form['suspend']['user_suspend_email_suspend'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send email on user suspend?'),
    '#default_value' => variable_get('user_suspend_email_suspend', 0),
  );
  $form['suspend']['user_suspend_email_suspend_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject of user suspend e-mail'),
    '#default_value' => variable_get('user_suspend_email_suspend_subject', 'Account suspended for !username at !site'),
    '#description' => t('Customize the subject of your user suspend e-mail. Available variables are: !username, !site, !uri, !login_url, !mailto, !date.'),
  );
  $form['suspend']['user_suspend_email_suspend_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of user suspend e-mail'),
    '#rows' => 8,
    '#cols' => 60,
    '#default_value' => variable_get('user_suspend_email_suspend_body', USER_SUSPEND_DEFAULT_EMAIL),
    '#description' => t('Customize the body of your user suspend e-mail. Available variables are: !username, !site, !uri, !login_url, !mailto, !date.'),
  );

  $form['reinstate'] = array('#type' => 'fieldset', '#title' => t('User reinstate e-mail settings'));
  $form['reinstate']['user_suspend_email_reinstate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send email on user reinstate?'),
    '#default_value' => variable_get('user_suspend_email_reinstate', 0),
  );
  $form['reinstate']['user_suspend_email_reinstate_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject of user reinstate e-mail'),
    '#default_value' => variable_get('user_suspend_email_reinstate_subject', 'Account reinstated for !username at !site'),
    '#description' => t('Customize the subject of your user reinstate e-mail. Available variables are: !username, !site, !uri, !login_url, !mailto, !date.'),
  );
  $form['reinstate']['user_suspend_email_reinstate_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body of user reinstate e-mail'),
    '#rows' => 8,
    '#cols' => 60,
    '#default_value' => variable_get('user_suspend_email_reinstate_body', USER_SUSPEND_REINSTATE_EMAIL),
    '#description' => t('Customize the body of your user reinstate e-mail. Available variables are: !username, !site, !uri, !login_url, !mailto, !date.'),
  );
  
  return system_settings_form($form);
}

/**
 * Theme user administration overview.
 */
function theme_user_suspend_admin($form) {
  // Overview table:
  $header = array(
    theme('table_select_header_cell'),
    array('data' => t('Username'), 'field' => 'u.name'),
    array('data' => t('Email'), 'field' => 'u.mail'),
    t('Roles'),
    t('Nodes'),
    t('Comments'),
    array('data' => t('Suspended'), 'field' => 'r.suspended', 'sort' => 'desc')
  );

  $output = drupal_render($form['options']);
  if (isset($form['name']) && is_array($form['name'])) {
    foreach (element_children($form['name']) as $key) {
      $rows[] = array(
        drupal_render($form['accounts'][$key]),
        drupal_render($form['name'][$key]),
        drupal_render($form['mail'][$key]),
        drupal_render($form['roles'][$key]),
        drupal_render($form['nodes'][$key]),
        drupal_render($form['comments'][$key]),
        drupal_render($form['suspended'][$key]),
      );
    }
  }
  else  {
    $rows[] = array(array('data' => t('No suspended users available.'), 'colspan' => '8'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

/**
 * Implementation of hook_user_operations().
 *
 * Suspend selected users.
 */
function user_suspend_user_operations() {
  global $form_values;
	if (!user_access('administer users')) return;
  $operations = array(
    'suspend' => array(
      'label' => t('Suspend the selected users'),
      'callback' => 'user_suspend_operations_suspend',
    ),
  );
	return $operations;
}

/**
 * Callback function for admin mass suspending users.
 */
function user_suspend_operations_suspend($accounts) {
  global $user;

  if (count($accounts)) {
    foreach ($accounts as $uid) {
      if ($uid == $user->uid) {
        drupal_set_message(t('You cannot suspend your own account.'), 'error');
        continue;
      }

      if ($user_name = db_result(db_query('SELECT name FROM {users} WHERE uid = %d', (int)$uid))) {
        if (_user_suspend_user($uid)) {
          drupal_set_message(t('Suspended user: %user.', array('%user' => $user_name)));
        }
        else {
          drupal_set_message(t('%user cannot be suspended.', array('%user' => $user_name)), 'error');
        }
      }
    }
  }
  else {
    drupal_set_message(t('No accounts selected to suspend.'), 'error');
  }
}

/**
 * Menu callback
 *
 * @return
 *   array of form content.
 */
function user_suspend_admin() {
  _user_suspend_schema_diff();

  $header = array(
    array(),
    array('data' => t('Username'), 'field' => 'u.name'),
    array('data' => t('Email'), 'field' => 'u.mail'),
    t('Roles'),
    t('Nodes'),
    t('Comments'),
    array('data' => t('Suspended'), 'field' => 'r.suspended', 'sort' => 'desc')
  );

  $sql = 'SELECT DISTINCT u.uid, u.name, u.mail, r.suspended, COUNT(n.nid) as nodes, COUNT(c.cid) as comments 
    FROM {user_suspend_users} u 
      LEFT JOIN {user_suspend} r USING (uid) 
      LEFT JOIN {user_suspend_users_roles} ur USING (uid) 
      LEFT JOIN {user_suspend_node} n USING (uid) 
      LEFT JOIN {user_suspend_comments} c USING (uid) 
        WHERE u.uid != 0 GROUP BY uid';
  $sql .= tablesort_sql($header);
  $query_count = 'SELECT COUNT(DISTINCT u.uid) FROM {user_suspend_users} u WHERE u.uid != 0';
  $result = pager_query($sql, 50, 0, $query_count);
  
  $roles = user_roles(1);

  while ($account = db_fetch_object($result)) {
    $accounts[$account->uid] = '';
    $form['name'][$account->uid] = array('#value' => $account->name);
    $form['mail'][$account->uid] = array('#value' => $account->mail);
    $users_roles = array();
    $roles_result = db_query('SELECT rid FROM {user_suspend_users_roles} WHERE uid = %d', $account->uid);
    while ($user_role = db_fetch_object($roles_result)) {
      $users_roles[] = $roles[$user_role->rid];
    }
    asort($users_roles);
    $form['roles'][$account->uid][0] = array('#value' => theme('item_list', $users_roles));
    $form['nodes'][$account->uid] = array('#value' => $account->nodes);
    $form['comments'][$account->uid] = array('#value' => $account->comments);
    $form['suspended'][$account->uid] =  array('#value' => $account->suspended ? t('@time ago', array('@time' => format_interval(time() - $account->suspended))) : t('never'));
  }
  $form['accounts'] = array(
    '#type' => 'checkboxes',
    '#options' => $accounts
  );

  $form['reinstate'] = array(
    '#type' => 'button', 
    '#value' => t('Reinstate Users'),
  );
  $form['delete'] = array(
    '#type' => 'button', 
    '#value' => t('Delete Users'),
  );
  $form['pager'] = array('#value' => theme('pager', NULL, 50));

  return $form;
}

/**
 * Validation of user suspend admin form.
 *
 * @param $form_id
 *   The unique string identifying the form.
 * @param $form_values
 *   An array of values mirroring the values returned by the form
 *   when it is submitted by a user.
 *
 * @return
 *   Any form validation errors encountered.
 */
function user_suspend_admin_validate($form_id, $form_values) {
  global $base_url;
  $time = time();
  $users_selected = array();
  foreach ($form_values['accounts'] as $uid => $value) {
    if ($value) {
      if ((int)$uid > 1) {
        $users_selected[] = $uid;
      }
    }
  }
  if (!sizeof($users_selected)) {
    form_set_error('', t('No users selected.'));
    return;
  }

  $delete = ($_POST['op'] == t('Delete Users')) ? true : false;
  foreach ($users_selected as $uid) {
    $user = db_fetch_array(db_query('SELECT name, mail FROM {user_suspend_users} WHERE uid = %d', $uid));
    $message = array('%user' => $user['name'], '%email' => '<'. $user['mail'] .'>');
    //sanity check
    $user_exists = db_result(db_query('SELECT uid FROM {users} WHERE uid = %d', $uid));
    if (!$user_exists) {
      if (_user_suspend_copy_table($uid, 'users')) {
        if (_user_suspend_copy_table($uid, 'users_roles')) {

          //nodes cannot be loaded without a valid user object!
          $result_nodes_delete = false;
          if (db_num_rows(db_query("SELECT * FROM {user_suspend_node} WHERE uid = %d", $uid))) { //nodes to work with
            if (variable_get('user_suspend_remove_nodes', 1)) { //enable remove nodes
              if (_user_suspend_copy_table($uid, 'node')) {
                $result_nodes_delete = true;
                if ($delete) {
                  $result = db_query("SELECT nid FROM {user_suspend_node} WHERE uid = %d", $uid);
                  while ($row = db_fetch_object($result)) {
                    node_delete($row->nid);
                  }
                }
                //delete user_suspend tables
                db_query('DELETE FROM {user_suspend_node} WHERE uid = %d', $uid);
              }
              else {
                drupal_set_message(t('%user nodes cannot be saved.', $message));
              }
            }
          }

          //comments cannot be loaded without a valid user object!
          $result_comments_delete = false;
          if (db_num_rows(db_query("SELECT * FROM {user_suspend_comments} WHERE uid = %d", $uid))) { //comments to work with
            if (variable_get('user_suspend_remove_comments', 1)) { //enable remove comments
              if (_user_suspend_copy_table($uid, 'comments')) {
                $result_comments_delete = true;
                if ($delete) {
                  $query = db_query('SELECT c.*, u.name AS registered_name, u.uid FROM {user_suspend_comments} c INNER JOIN {users} u ON u.uid = c.uid WHERE c.uid = %d', $uid);
                  while ($comment = db_fetch_object($query)) {
                    _comment_delete_thread($comment); //deletes comment and all replies!
                  }
                }
                //delete user_suspend tables
                db_query('DELETE FROM {user_suspend_comments} WHERE uid = %d', $uid);
              }
              else {
                drupal_set_message(t('%user comments cannot be saved.', $message));
              }
            }
          }

          //delete user_suspend tables
          db_query('DELETE FROM {user_suspend} WHERE uid = %d', $uid);
          db_query('DELETE FROM {user_suspend_users} WHERE uid = %d', $uid);
          db_query('DELETE FROM {user_suspend_users_roles} WHERE uid = %d', $uid);

          if ($delete) {
            user_delete(array('confirm' => 1), $uid);
            //watchdog and messages supplied by node_delete and user_delete
          }
          else { //reinstate user
            drupal_set_message(t('Reinstated user: %user %email.', $message));
            watchdog('user suspend', t('Reinstated user: %user %email.', $message), WATCHDOG_NOTICE);
            if ($result_nodes_delete) {
              drupal_set_message(t('%user nodes restored.', $message));
            }
            if ($result_comments_delete) {
              drupal_set_message(t('%user comments restored.', $message));
            }
            //user notify
            if (variable_get('user_suspend_email_reinstate', 0)) {
              $from = variable_get('site_mail', ini_get('sendmail_from'));
              $variables = array('!username' => $user['name'], '!site' => variable_get('site_name', 'Drupal'), '!uri' => $base_url, '!login_url' => url('user', NULL, NULL, TRUE), '!mailto' => $user['mail'], '!date' => format_date($time));
              $subject = strtr(variable_get('user_suspend_email_reinstate_subject', 'Account reinstated for !username at !site'), $variables);
              $body = strtr(variable_get('user_suspend_email_reinstate_body', USER_SUSPEND_REINSTATE_EMAIL), $variables);
              $mail_success = drupal_mail('user-suspend', $user['mail'], $subject, $body, $from);
              if ($mail_success) {
                watchdog('user suspend', t('Account reinstate notification mailed to %user at %email.', $message));
              }
              else {
                watchdog('user suspend', t('Error mailing account reinstate notification to %user at %email.', $message), WATCHDOG_ERROR);
              }
            }
          }
        }
        else {
          drupal_set_message(t('%user roles cannot be saved.', $message), 'error');
        }

      }
      else {
        drupal_set_message(t('%user cannot be saved.', $message), 'error');
      }
    }
    else {
      drupal_set_message(t('%user already exists.', $message), 'error');
    }
  } //end foreach

  if ($delete) {
    drupal_goto('admin/user/user/suspend');
  }
  else {
    drupal_goto('admin/user/user');
  }

}

/**
 * Implementation of hook_form_alter().
 */
function user_suspend_form_alter($form_id, &$form) {
  global $form_values;

  if ($form_id == 'user_register') {
    $form['#validate'] = array('user_suspend_user_register_validate' => array()) + (array)$form['#validate'];
  }
}

/**
 * Validation of hook_form_alter().
 *
 * User registration hook to prevent registration with suspended email address. 
 */

function user_suspend_user_register_validate($form_id, $form_values) {
  if (db_num_rows(db_query("SELECT uid FROM {user_suspend_users} WHERE LOWER(name) = LOWER('%s')", $form_values['name'])) > 0) {
    form_set_error('name', t('The name %name is already taken.', array('%name' => $form_values['name'])));
  }
  if (db_num_rows(db_query("SELECT uid FROM {user_suspend_users} WHERE LOWER(mail) = LOWER('%s')", $form_values['mail'])) > 0) {
    form_set_error('mail', t('An account with this e-mail address has already been suspended.'));
  }
}

/**
 * Suspend user.
 *
 * @param $uid
 *   The user ID of the user to suspend.
 *
 * @return
 *   true is success, false if user does not exist.
 */
function _user_suspend_user($uid) {
  global $base_url;
  $uid = (int)$uid;
  $time = time();

  //sanity check
  if ($uid < 1) {
    watchdog('user suspend', t('Invalid User ID: %uid.', array('%uid' => $uid)), WATCHDOG_ERROR);
    return false;
  }
  elseif ($uid == 1) {
    watchdog('user suspend', t('Cannot suspend superuser'), WATCHDOG_ERROR);
    return false;
  }

  $account = db_fetch_array(db_query('SELECT name, mail FROM {users} WHERE uid = %d', $uid));
  $message = array('%user' => $account['name'], '%email' => '<'. $account['mail'] .'>');
  if (!$account['name']) {
    watchdog('user suspend', t('Invalid User ID: %uid.', array('%uid' => $uid)), WATCHDOG_ERROR);
    return false;
  }

  if (db_result(db_query('SELECT uid FROM {user_suspend_users} WHERE uid = %d', $uid))) {
    watchdog('user suspend', t('User already suspended: %user %email.', $message), WATCHDOG_ERROR);
    return false;
  }

  if (_user_suspend_copy_table($uid, 'users', 'user_suspend')) {
    if (_user_suspend_copy_table($uid, 'users_roles', 'user_suspend')) {
      //nodes
      $result_nodes_delete = false;
      $result_nodes_orphan = false;
      if (db_num_rows(db_query("SELECT * FROM {node} WHERE uid = %d", $uid))) { //nodes to work with
        if (variable_get('user_suspend_remove_nodes', 1)) { //enable remove nodes
          if (_user_suspend_copy_table($uid, 'node', 'user_suspend')) {
            $result_nodes_delete = db_query('DELETE FROM {node} WHERE uid = %d', $uid);
          }
          else {
            watchdog('user suspend', t('User nodes cannot be saved: %user %email.', $message), WATCHDOG_ERROR);
          }
        }
        else { //orphan nodes
          $result_nodes_orphan = true;
          db_query('UPDATE {node} SET uid = 0 WHERE uid = %d', $uid);
          db_query('UPDATE {node_revisions} SET uid = 0 WHERE uid = %d', $uid);
        }
      }
      if ($result_nodes_delete) {
        watchdog('user suspend', t('User nodes removed: %user %email.', $message), WATCHDOG_NOTICE);
      }
      elseif ($result_nodes_orphan) {
        watchdog('user suspend', t('User nodes orphaned: %user %email.', $message), WATCHDOG_NOTICE);
      }

      //comments: needs work to handle replies... when a comment is deleted, the entire thread is deleted
      $result_comments_delete = false;
      $result_comments_orphan = false;
      if (db_num_rows(db_query("SELECT * FROM {comments} WHERE uid = %d", $uid))) { //comments to work with
        if (variable_get('user_suspend_remove_comments', 1)) { //enable remove comments
          if (_user_suspend_copy_table($uid, 'comments', 'user_suspend')) {
            $result_comments_delete = db_query('DELETE FROM {comments} WHERE uid = %d', $uid);
          }
          else {
            watchdog('user suspend', t('User comments cannot be saved: %user %email.', $message), WATCHDOG_ERROR);
          }
        }
        else { //orphan comments
          $result_comments_orphan = true;
          db_query('UPDATE {comments} SET uid = 0 WHERE uid = %d', $uid);
        }
        db_query('UPDATE {node_comment_statistics} SET last_comment_uid = 0 WHERE last_comment_uid = %d', $uid);
      }
      if ($result_comments_delete) {
        watchdog('user suspend', t('User comments removed: %user %email.', $message), WATCHDOG_NOTICE);
      }
      elseif ($result_comments_orphan) {
        watchdog('user suspend', t('User comments orphaned: %user %email.', $message), WATCHDOG_NOTICE);
      }

      //user cleanup
      sess_destroy_uid($uid);
      db_query('DELETE FROM {users} WHERE uid = %d', $uid);
      db_query('DELETE FROM {users_roles} WHERE uid = %d', $uid);
      db_query('INSERT INTO {user_suspend} (uid,suspended) VALUES (%d,%d)', $uid, $time);
      //user notify
      if (variable_get('user_suspend_email_suspend', 0)) {
        $from = variable_get('site_mail', ini_get('sendmail_from'));
        $variables = array('!username' => $account['name'], '!site' => variable_get('site_name', 'Drupal'), '!uri' => $base_url, '!login_url' => url('user', NULL, NULL, TRUE), '!mailto' => $account['mail'], '!date' => format_date($time));
        $subject = strtr(variable_get('user_suspend_email_suspend_subject', 'Account suspended for !username at !site'), $variables);
        $body = strtr(variable_get('user_suspend_email_suspend_body', USER_SUSPEND_DEFAULT_EMAIL), $variables);
        $mail_success = drupal_mail('user-suspend', $account['mail'], $subject, $body, $from);
        if ($mail_success) {
          watchdog('user suspend', t('Account suspension notification mailed to %user at %email.', $message));
        }
        else {
          watchdog('user suspend', t('Error mailing account suspension notification to %user at %email.', $message), WATCHDOG_ERROR);
        }
      }
      watchdog('user suspend', t('Suspended user: %user %email.', $message), WATCHDOG_NOTICE);
      return true;
    }
    else {
      watchdog('user suspend', t('User roles cannot be saved: %user %email.', $message), WATCHDOG_ERROR);
      return false;
    }
  }
  else {
    watchdog('user suspend', t('User cannot be saved: %user %email.', $message), WATCHDOG_ERROR);
    return false;
  }
}

/**
 * Internal functions
 */
function _user_suspend_copy_table($uid, $table = 'users', $to_table = '') {
  if (!(int)$uid) return false;
  if (!in_array($table, array('node', 'comments', 'users', 'users_roles'))) return false;
  if ($to_table == '') {
    $from_table = 'user_suspend_';
  }
  else if ($to_table == 'user_suspend') {
    $to_table = 'user_suspend_';
    $from_table = '';
  }
  else return false;

  $schema = implode(',', _user_suspend_get_drupal_schema($table));
  return db_query('INSERT INTO {'. $to_table . $table . '} (' . $schema . ') SELECT ' . $schema . ' FROM {' . $from_table . $table . '} WHERE uid = %d', $uid);
}

function _user_suspend_schema_diff() {
  $tables = array('node', 'comments', 'users', 'users_roles');
  foreach ($tables as $table) {
    $schema_diff = array_diff(_user_suspend_get_drupal_schema($table, FALSE), _user_suspend_get_drupal_schema($table));
    if (count($schema_diff)) {
      drupal_set_message(t('User Suspend does not recognize table %table as default Drupal schema and cannot preserve data from fields %fields when suspending users.', array('%table' => $table, '%fields' => implode(" and ", $schema_diff))), 'warning');
    }
  }
}

function _user_suspend_get_drupal_schema($table = 'node', $default = TRUE) {
  if (in_array($table, array('node', 'comments', 'users', 'users_roles'))) {
    if ($default) {
      if ($table == 'node') return array('nid', 'vid', 'type', 'title', 'uid', 'status', 'created', 'changed', 'comment', 'promote', 'moderate', 'sticky');
      if ($table == 'comments') return array('cid', 'pid', 'nid', 'uid', 'subject', 'comment', 'hostname', 'timestamp', 'score', 'status', 'format', 'thread', 'users', 'name', 'mail', 'homepage');
      if ($table == 'users_roles') return array('uid', 'rid');
      return array('uid', 'name', 'pass', 'mail', 'mode', 'sort', 'threshold', 'theme', 'signature', 'created', 'access', 'login', 'status', 'timezone', 'language', 'picture', 'init', 'data');
    }
    //get dynamic schema
    $result = db_query('SELECT * FROM {' . $table . '} LIMIT 0,1');
    if (db_num_rows($result)) {
      return array_keys(db_fetch_array($result));
    }
  }
  return array();
}
