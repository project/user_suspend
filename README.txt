

Description
-----------
User Suspend provides an interim step between account blocking and account 
deletion. The user record, user roles and optionally user-created nodes and 
comments are transferred to separate tables where they are not accessible by
the normal Drupal system. Suspended users can be reinstated at any time, 
recovering all of their previous settings, roles, nodes, and comments.


Prerequisites
-------------
None


Installation
------------
1. copy the user_suspend directory and all its contents to your 
   modules directory.
2. enable the module: admin/build/modules. The module will install five tables
   into your database.
3. configure the module: admin/user/settings/user_suspend


Usage
-------------
Suspend users from the default Users list (admin/user/user) by selecting 
'Suspend the selected users' from the Update options. View, reinstate and 
delete suspended users from the Suspended Users list (admin/user/user/suspend).


Known Issues
-------------
Comments are finicky. When comments from suspended users are removed or 
restored, the comment count may be inaccurate until a new comment is added. 
When comments are deleted, any comments threaded below a comment from a 
suspended user will also be deleted. This is by design of the Drupal 
comment.module to eliminate orphaned comments.

Workaround: 
  Disable 'Remove user comments?' in the User suspend settings, which will 
  detach all comments from a suspended user (uid set to 0). The drawback is 
  that suspended user comments will not be able to be reattached to the user if
  reinstated in the future.

Modules which add additional columns to the core node, comments or users tables
will lose data from these columns if a user is suspended and then unsuspended. 
We had considered replicating the changes to these core tables in the 
corresponding user suspend table, but ultimately decided it was not good 
practice to support every (any?) module that chooses to alter these tables.


Bugs/Features/Patches:
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so 
at the project page on the Drupal web site.
http://drupal.org/project/user_suspend


Changlog:
----------
1.2  added check to prevent suspending self
     streamlined table copy functions to support only default schema data
     fixed install error for pgsql db
1.1  fixed bulk user reinstate/delete from User Suspend list
1.0  initial release


Todo List:
----------
None


Author
------
John Money
ossemble LLC.
http://ossemble.com

Module development sponsored by LifeWire, a subsidiary of The New York Times 
Company.
http://www.lifewire.com
